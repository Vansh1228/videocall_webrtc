import express from "express";
import path from "path";
import http from "http";
import { Server } from "socket.io";

const PORT_SOCKET = 8000;
const PORT_EXPRESS = 8443;
const app = express();
const __dirname = path.dirname(new URL(import.meta.url).pathname);
app.use("/", express.static(path.resolve(__dirname, "dist")));
const serverExpress = http.createServer(app);
const userNameToSocketIdMap = new Map();
const socketidTouserNameMap = new Map();

const serverSocket = http.createServer(serverExpress);
const io = new Server(serverSocket, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
    credentials: true,
  },
});

let room;

io.on("connection", (socket) => {
  console.log(`Socket Connected`, socket.id);
  socket.on("room-joined", (data) => {
    const { userName, roomId } = data;
    console.log(data);
    room = roomId;
    userNameToSocketIdMap.set(userName, socket.id);
    socketidTouserNameMap.set(socket.id, userName);
    console.log("MAP LENGTH", userNameToSocketIdMap.size);

    socket.on("getLocalSocketId", (data) => {
      io.to(roomId).emit("setLocalSocketId", data);
    });

    console.log("SOCKET ID", socket.id);

    io.to(roomId).emit("user:joined", { userName, id: socket.id });

    socket.join(roomId);
    console.log(userNameToSocketIdMap);
    io.to(socket.id).emit("room-joined", data);
    socket.on("receive-message", (data) => {
      console.log("DATA MESSAGE", data);

      io.to(roomId).emit("display-message", {
        id: socket.id,
        userName: socketidTouserNameMap.get(data.from),
        message: data.message,
      });
    });
  });
  socket.on("user:left", (id) => {
    console.log("IDD", id);
    socketidTouserNameMap.delete(id.id);
    console.log(socketidTouserNameMap);
  });
  socket.on("user-call", ({ to, offer }) => {
    console.log("OFFER", offer);

    io.to(to).emit("incomming-call", {
      from: socket.id,
      offer,
      localUser: socketidTouserNameMap.get(socket.id),
    });
  });
  socket.on("checkRemote", ({ to }) => {
    io.to(to).emit("remoteUser", {
      localUser: socketidTouserNameMap.get(socket.id),
    });
  });
  socket.on("call-accepted", ({ to, ans }) => {
    console.log("ANSWER", ans);
    io.to(to).emit("call-accepted", {
      from: socket.id,
      ans,
      remoteUser: socketidTouserNameMap.get(socket.id),
    });
  });

  socket.on("peer-nego-needed", ({ to, offer }) => {
    console.log("peer-nego-needed", offer);
    io.to(to).emit("peer-nego-needed", { from: socket.id, offer });
  });

  socket.on("peer-nego-done", ({ to, ans }) => {
    console.log("peer-nego-done", ans);
    io.to(to).emit("peer-nego-final", { from: socket.id, ans });
  });
  socket.on("stream:ended", (data) => {
    console.log(data);
    io.to(room).emit("stream:ended", socketidTouserNameMap.get(data));
  });
  socket.on("disconnect", () => {
    socket.disconnect(true);
  });
});

serverSocket.listen(PORT_SOCKET, () => {
  console.log(`SERVER CREATED AND UP AND RUNNING ON PORT ${PORT}`);
});

serverExpress.listen(PORT_EXPRESS, () =>
  console.log(`Listening to https server for express on [::]:8443`)
);

import React, { useEffect, useCallback, useState } from "react";
import ReactPlayer from "react-player";
import Peer from "../Service/Peer.jsx";
import { useSocket } from "../Context/Socket.jsx";
import { useNavigate } from "react-router-dom";

const Room = () => {
  const socket = useSocket();
  const navigate = useNavigate();
  const [remoteSocketId, setRemoteSocketId] = useState(null);
  const [myStream, setMyStream] = useState();
  const [remoteStream, setRemoteStream] = useState();
  const [remoteUserName, setRemoteUserName] = useState();
  const [localUserName, setLocalUserName] = useState();
  const [message, setMessage] = useState("");
  const [storedMessages, setStoredMessages] = useState([]);
  const [chat, setChat] = useState(false);
  const [videoEnabled, setVideoEnabled] = useState(true);
  const [audioEnabled, setAudioEnabled] = useState(true);

  const toggleVideo = () => {
    if (myStream) {
      const videoTracks = myStream.getVideoTracks();
      if (videoTracks.length > 0) {
        const videoTrack = videoTracks[0];
        videoTrack.enabled = !videoTrack.enabled;
        setVideoEnabled(videoTrack.enabled);
      }
    }
  };

  const toggleAudio = () => {
    if (myStream) {
      const audioTracks = myStream.getAudioTracks();
      audioTracks.forEach(track => {
        track.enabled = !track.enabled;
      });
      setAudioEnabled(prev => !prev);
    }
  };

  const handleCallUser = useCallback(async () => {
    try {
      const stream = await navigator.mediaDevices.getUserMedia({
        audio: true,
        video: true,
      });
      const offer = await Peer.getOffer();
      console.log("Offer SDP:", offer);
      socket.emit("user-call", { to: remoteSocketId, offer });
      setMyStream(stream);
    } catch (error) {
      console.error("Error generating offer:", error);
    }
  }, [remoteSocketId, socket]);

  const handleIncommingCall = useCallback(
    async ({ from, offer, localUser }) => {
      try {
        setLocalUserName(localUser);
        setRemoteSocketId(from);
        const stream = await navigator.mediaDevices.getUserMedia({
          audio: true,
          video: true,
        });
        setMyStream(stream);
        console.log("Received Answer SDP:", offer.sdp);
        const ans = await Peer.getAnswer(offer);
        socket.emit("call-accepted", { to: from, ans });
      } catch (error) {
        console.error("Error handling incoming call:", error);
      }
    },
    [socket]
  );

  const sendStreams = useCallback(() => {
    socket.emit("checkRemote", { to: remoteSocketId });
    for (const track of myStream.getTracks()) {
      Peer.peer.addTrack(track, myStream);
    }
    socket.on("remoteUser", ({ localUser }) => {
      setLocalUserName(localUser);
    });
  }, [myStream, localUserName]);

  const handleCallAccepted = useCallback(
    ({ ans }) => {
      Peer.setLocalDescription(ans);
      console.log("Call Accepted!", ans);
      sendStreams();
    },
    [sendStreams]
  );

  const handleNegoNeeded = useCallback(async () => {
    const offer = await Peer.getOffer();
    socket.emit("peer-nego-needed", { to: remoteSocketId, offer });
  }, [remoteSocketId, socket]);

  useEffect(() => {
    Peer.peer.addEventListener("negotiationneeded", handleNegoNeeded);
    return () => {
      Peer.peer.removeEventListener("negotiationneeded", handleNegoNeeded);
    };
  }, [handleNegoNeeded]);

  const handleNegoNeedIncomming = useCallback(
    async ({ from, offer }) => {
      const ans = await Peer.getAnswer(offer);
      socket.emit("peer-nego-done", { to: from, ans });
    },
    [socket]
  );

  const handleNegoNeedFinal = useCallback(async ({ ans }) => {
    await Peer.setLocalDescription(ans);
  }, []);

  const handleSubmitChat = (e) => {
    e.preventDefault();
    socket.emit("receive-message", { from: socket.id, message });
    setMessage("");
  };

  const toggleChat = (e) => {
    e.preventDefault();
    setChat(!chat);
  };

  useEffect(() => {
    Peer.peer.addEventListener("track", async (ev) => {
      const remoteStream = ev.streams;
      console.log("GOT TRACKS!!");
      setRemoteStream(remoteStream[0]);
    });
    socket.on("display-message", handleChatMessage);

    return () => {
      socket.off("display-message", handleChatMessage);
      if (myStream) {
        myStream.getTracks().forEach((track) => {
          track.stop();
        });
      }
    };
  }, [myStream]);

  const handleUserJoined = ({ userName, id }) => {
    console.log(`Username ${userName} joined room and your socket ID is ${id}`);
    setRemoteUserName(userName);
    setRemoteSocketId(id);
  };

  const handleEndCall = () => {
    console.log("END CALL PRESSED");
    if (myStream) {
      myStream.getTracks().forEach((track) => {
        track.stop();
      });
    }
    socket.emit("user:left", { id: socket.id });
    socket.emit("stream:ended", { myStream, remoteUser: socket.id });
    navigate("/");
  };

  const handleChatMessage = (messageData) => {
    setStoredMessages((prev) => [...prev, messageData]);
  };

  const handleEndStream = ({ myStream, remoteUser }) => {
    setRemoteUserName(remoteUser);
    setRemoteStream(myStream);
  };

  useEffect(() => {
    console.log("LOCAL USER FROM MYSTREAM", localUserName);
    console.log("REMOTE USER FROM MYSTREAM", remoteUserName);
    console.log("SOCKET", socket);
    console.log("MY STREAM", myStream);
    console.log("REMOTE STREAM", remoteStream);

    socket.on("user:joined", handleUserJoined);
    socket.on("incomming-call", handleIncommingCall);
    socket.on("call-accepted", handleCallAccepted);
    socket.on("peer-nego-needed", handleNegoNeedIncomming);
    socket.on("peer-nego-final", handleNegoNeedFinal);
    socket.on("stream:ended", handleEndStream);

    return () => {
      socket.off("user:joined", handleUserJoined);
      socket.off("incomming-call", handleIncommingCall);
      socket.off("call-accepted", handleCallAccepted);
      socket.off("peer-nego-needed", handleNegoNeedIncomming);
      socket.off("peer-nego-final", handleNegoNeedFinal);
    };
  }, [
    socket,
    handleUserJoined,
    handleIncommingCall,
    handleCallAccepted,
    handleNegoNeedIncomming,
    handleNegoNeedFinal,
  ]);

  return (
    <>
      <div>
        <h1>Room Page</h1>
        <h4>
          {remoteSocketId ? (
            <>
              "Connected"
              <br />
              {localUserName} has joined the room
              {myStream ? (
                <button onClick={handleEndCall}>END CALL</button>
              ) : (
                <button onClick={handleCallUser}>CALL</button>
              )}
            </>
          ) : (
            "No one in room"
          )}
        </h4>
      </div>

      <div className="StreamContainer">
        <div className="myStream">
          {myStream && (
            <>
              <button onClick={sendStreams}>SEND MY STREAM</button>
              <h2>You are on Videocall with {localUserName} </h2>{" "}
              <h1>YOU</h1>
              <button onClick={toggleVideo}>
                {videoEnabled ? "Turn Off Video" : "Turn On Video"}
              </button>
              <button onClick={toggleAudio}>
                {audioEnabled ? "Turn Off Audio" : "Turn On Audio"}
              </button>
              <ReactPlayer
                playing
                muted={!audioEnabled}
                height="300px"
                width="500px"
                url={myStream}
              />
            </>
          )}
        </div>
        <div className="remoteStream">
          {remoteStream && (
            <>
              <h1>{localUserName}</h1>
              <ReactPlayer
                playing
                muted={!audioEnabled}
                height="300px"
                width="500px"
                url={remoteStream}
              />
            </>
          )}
        </div>
      </div>
      <button onClick={toggleChat}>CHAT</button>
      {chat && (
        <div className="chat">
          <h2>Chat Section</h2>
          <input
            type="text"
            placeholder="type here..."
            value={message}
            onChange={(e) => {
              setMessage(e.target.value);
            }}
          />
          <button onClick={handleSubmitChat}>Send</button>
          <ul>
            {storedMessages.map(({ userName, message }, index) => (
              <li key={index}>
                {userName}: {message}
              </li>
            ))}
          </ul>
        </div>
      )}
    </>
  );
};

export default Room;

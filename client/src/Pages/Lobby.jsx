import React, { useState, useEffect, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import { useSocket } from "../Context/Socket";

function Lobby() {
  const [userName, setUserName] = useState("");
  const [roomId, setRoomId] = useState("");
  const socket = useSocket();
  const navigate = useNavigate();

  const handleSubmit = useCallback(() => {
    socket.emit("room-joined", { userName, roomId });
  }, [userName, roomId, socket]);

  const handleJoinRoom = useCallback((data) => {
    const { userName, roomId } = data;
    navigate(`/room/${roomId}`);
  }, [navigate]);

  useEffect(() => {
    socket.on("room-joined", handleJoinRoom);
    return () => {
      socket.off("room-joined", handleJoinRoom);
    };
  }, [socket, handleJoinRoom]);

  return (
    <div>
      <input
        type="text"
        placeholder="Enter Username"
        value={userName}
        onChange={(e) => setUserName(e.target.value)}
      />
      <input
        type="text"
        placeholder="Enter Room ID"
        value={roomId}
        onChange={(e) => setRoomId(e.target.value)}
      />
      <button onClick={handleSubmit}>Join Chat</button>
    </div>
  );
}

export default Lobby;

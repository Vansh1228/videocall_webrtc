import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Room from "./Pages/Room";
import Lobby from "./Pages/Lobby";
function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Lobby />}></Route>
        <Route path="/room/:id" element={<Room />}></Route>
      </Routes>
    </Router>
  );
}

export default App;

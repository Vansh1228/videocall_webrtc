import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { Socket } from "./Context/Socket.jsx";

ReactDOM.createRoot(document.getElementById("root")).render(
  <Socket>
    <App />
  </Socket>
);
